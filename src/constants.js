export const SPACING = 8;
export const BIG_WIDTH = 8;
export const SMALL_WIDTH = 3;
export const SUCCES_TIMEOUT = 3500;

export const SEPARATORS = {
  comma: ',',
  space: ' ',
  commaSpace: ', ',
}
export const DEFAULT_SEPARATOR = 'comma';
