import React from 'react';
import { IntlProvider } from 'react-intl';
import App from './App';
import messages from './i18n';
import { flattenObject } from './tools';

import './css/index.less';
import { createRoot } from 'react-dom/client';

let language = navigator.language.substr(0, 2);
if (!messages[language]) {
  language = 'en';
}

const flatMessages = flattenObject(messages[language]);

const rootElement = document.querySelector('#app');
const root = createRoot(rootElement);

root.render(
  <IntlProvider locale={language} messages={flatMessages}>
    <App />
  </IntlProvider>,

);
