import { createStore } from 'redux';

let initialState = {
  tempo: {
    data: [],
    parts: {},
    send: true,
    tempo: 120,
  },
  key: {
    data: [],
    parts: {},
    send: true,
  },
  harmony: {
    data: [],
    parts: {},
    send: true,
  },
  pitchCorrection: {
    data: [],
    parts: {},
    send: false,
  },
  separator: null,
};

let reducer = (state = initialState, action) => {
  switch (action.type) {
    case "TEMPO_CHANGED": {
      const { tempo } = state;
      tempo.data = action.tempoData || [];
      return { ...state, tempo };
      break;
    }
    case "TEMPO_TEMPO_CHANGED": {
      const { tempo } = state;
      tempo.tempo = action.tempo;
      return { ...state, tempo };
      break;
    }
    case "TEMPO_SEND_CHANGED": {
      const { tempo } = state;
      tempo.send = action.send;
      return { ...state, tempo };
      break;
    }
    case "TEMPO_SET_PARTS": {
      const { tempo } = state;
      tempo.parts = action.tempoParts || {};
      return { ...state, tempo };
      break;
    }
    case "KEY_CHANGED": {
      const { key } = state;
      key.data = action.keyData || [];
      return { ...state, key };
      break;
    }
    case "KEY_SEND_CHANGED": {
      const { key } = state;
      key.send = action.send;
      return { ...state, key };
      break;
    }
    case "KEY_SET_PARTS": {
      const { key } = state;
      key.parts = action.keyParts || {};
      return { ...state, key };
      break;
    }
    case "HARMONY_CHANGED": {
      const { harmony } = state;
      harmony.data = action.harmonyData || [];
      return { ...state, harmony };
      break;
    }
    case "HARMONY_SEND_CHANGED": {
      const { harmony } = state;
      harmony.send = action.send;
      return { ...state, harmony };
      break;
    }
    case "HARMONY_SET_PARTS": {
      const { harmony } = state;
      harmony.parts = action.harmonyParts || {};
      return { ...state, harmony };
      break;
    }
    case "PITCH_CORRECTION_CHANGED": {
      const { pitchCorrection } = state;
      pitchCorrection.data = action.pitchCorrectionData || [];
      return { ...state, pitchCorrection };
      break;
    }
    case "PITCH_CORRECTION_SEND_CHANGED": {
      const { pitchCorrection } = state;
      pitchCorrection.send = action.send;
      return { ...state, pitchCorrection };
      break;
    }
    case "PITCH_CORRECTION_SET_PARTS": {
      const { pitchCorrection } = state;
      pitchCorrection.parts = action.pitchCorrectionParts || {};
      return { ...state, pitchCorrection };
      break;
    }
    case "SEPARATOR_CHANGE": {
      const { separator } = action;
      return { ...state, separator };
      break;
    }
  }
}


export let store = createStore(reducer);
