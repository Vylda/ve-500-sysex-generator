import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import SysexOutput from './sysexOutput';
import Send from './send';
import { store } from '../store';
import { SPACING, BIG_WIDTH, SMALL_WIDTH } from '../constants';
import { Grid2, Slider, TextField, Typography } from '@mui/material';

const KEY_NAMES = [
  'C',
  'D♭',
  'D',
  'E♭',
  'E',
  'F',
  'F♯',
  'G',
  'A♭',
  'A',
  'B♭',
  'H',
  'Cmi',
  'C♯mi',
  'Dmi',
  'D♯mi',
  'Emi',
  'Fmi',
  'F♯mi',
  'Gmi',
  'G♯mi',
  'Ami',
  'B♭mi',
  'Hmi',
];

const KEYS = {
  INITIAL: 0,
  MIN: 0,
  MAX: KEY_NAMES.length - 1,
};

const SYSEX_PARTS = {
  START: [
    'F0',
    '41',
    '10',
    '00',
    '00',
    '00',
    '55',
    '12',
    '10',
    '02',
    '40',
    '01',
  ],
  DATA: ['00', '00'],
  END: ['F7'],
};

class KeySysexOutput extends SysexOutput {
  subscribe() {
    store.subscribe(() => {
      let state = store.getState();
      this.setState({ sysex: this.makeSysex(state.key.data) });
    });
  }
}

const InjectedKeySysexOutput = injectIntl(KeySysexOutput);

class Key extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      key: KEYS.INITIAL,
      sysex: this.computeSysex(KEYS.INITIAL),
      send: true,
    };
    store.dispatch({ type: 'KEY_SET_PARTS', keyParts: SYSEX_PARTS });
    store.dispatch({ type: 'KEY_SEND_CHANGED', send: this.state.send });

    this.marks = [];
    KEY_NAMES.forEach((key, index) => {
      this.marks.push({
        value: index,
        label: key,
      });
    });

    this._intl = props.intl;
  }

  handleKeyChange(newValue) {
    const newKey = Math.min(Math.max(KEYS.MIN, Number(newValue)), KEYS.MAX);

    this.setState({
      key: newKey,
      keyName: KEY_NAMES[newKey],
      sysex: this.computeSysex(newKey),
    });
  }

  handleSendChange(send) {
    this.setState({
      send,
    });
    store.dispatch({ type: 'KEY_SEND_CHANGED', send });
  }

  computeSysex(value) {
    const data = this.getSysexData(value);
    store.dispatch({ type: 'KEY_CHANGED', keyData: data });
    return data;
  }

  getSysexData(value) {
    const lastBytes = SYSEX_PARTS.START.slice(-4);

    const checksumPart = lastBytes.reduce((acc, byte) => {
      const number = parseInt(Number(`0x${byte}`), 10)
      return acc + number;
    }, value);
    const checksum = 128 - checksumPart;

    const hexArray = [
      this.addZero(value.toString(16)),
      this.addZero(checksum.toString(16)),
    ];
    return this.updateSysexData(hexArray);
  }

  addZero(hexValue) {
    if (hexValue.length < 2) {
      return `0${hexValue}`;
    }
    return hexValue;
  }

  updateSysexData(newData) {
    if (newData.length >= SYSEX_PARTS.DATA) {
      return [...newData];
    }

    const head = SYSEX_PARTS.DATA.slice(
      0,
      SYSEX_PARTS.DATA.length - newData.length,
    );
    return [...head, ...newData];
  }
  componentDidMount() {
    this.computeSysex(this.state.key);
  }

  render() {
    return (
      <>
        <Typography id="key-slider" variant="h3" gutterBottom>
          <FormattedMessage id="key" />
        </Typography>
        <Send
          send={this.state.send}
          onChange={(data) => this.handleSendChange(data)}
        />
        <Grid2
          className="controlsSubmodule two-columns"
          container
          spacing={SPACING}>
          <Grid2 size={BIG_WIDTH}>
            <Slider
              value={this.state.key}
              aria-labelledby="key-slider"
              valueLabelDisplay="auto"
              valueLabelFormat={(x) => KEY_NAMES[x]}
              step={1}
              min={KEYS.MIN}
              max={KEYS.MAX}
              marks={this.marks}
              onChange={(event, value) => {
                this.handleKeyChange(value);
              }}
            />
          </Grid2>

          <Grid2 size={SMALL_WIDTH}>
            <TextField
              id="key-field"
              label={this._intl.formatMessage({ id: 'key' })}
              value={KEY_NAMES[this.state.key]}
              slotProps={{
                htmlInput: {
                  readOnly: true,
                },
              }}
            />
          </Grid2>
        </Grid2>

        <InjectedKeySysexOutput
          startData={SYSEX_PARTS.START}
          endData={SYSEX_PARTS.END}
          data={SYSEX_PARTS.DATA}
        />
      </>
    );
  }
}

export default injectIntl(Key);
