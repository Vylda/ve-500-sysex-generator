import React from 'react';
import SysexOutput from './sysexOutput';
import { FormattedMessage, injectIntl } from 'react-intl';

import Send from './send';
import { store } from '../store';
import { BIG_WIDTH } from '../constants';
import { Grid2, Switch, Typography } from '@mui/material';

const SYSEX_PARTS = {
  START: [
    'F0',
    '41',
    '10',
    '00',
    '00',
    '00',
    '55',
    '12',
    '20',
    '00',
    '30',
    '00',
  ],
  DATA: ['00', '30'],
  END: ['F7'],
};

class PitchCorrectionSysexOutput extends SysexOutput {
  subscribe() {
    store.subscribe(() => {
      let state = store.getState();
      this.setState({ sysex: this.makeSysex(state.pitchCorrection.data) });
    });
  }
}

const InjectedPitchCorrectionSysexOutput = injectIntl(PitchCorrectionSysexOutput);

class PitchCorrection extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pitchCorrectionOn: false,
      sysex: this.computeSysex(false),
      send: false,
    };

    store.dispatch({ type: 'PITCH_CORRECTION_SET_PARTS', pitchCorrectionParts: SYSEX_PARTS });
    store.dispatch({ type: 'PITCH_CORRECTION_SEND_CHANGED', send: this.state.send });
  }

  handlePitchCorrectionChange(event) {
    const pitchCorrectionEnabled = event.currentTarget.checked;
    this.setState({
      pitchCorrectionOn: pitchCorrectionEnabled,
      sysex: this.computeSysex(pitchCorrectionEnabled),
    });
  }

  handleSendChange(send) {
    this.setState({
      send,
    });
    store.dispatch({ type: 'PITCH_CORRECTION_SEND_CHANGED', send });
  }

  computeSysex(value) {
    const data = this.getSysexData(value);
    store.dispatch({ type: 'PITCH_CORRECTION_CHANGED', pitchCorrectionData: data });
    return data;
  }

  getSysexData(value) {
    const hexArray = value ? ['01', '2F'] : ['00', '30'];
    return this.updateSysexData(hexArray);
  }
  updateSysexData(newData) {
    if (newData.length >= SYSEX_PARTS.DATA) {
      return [...newData];
    }

    const head = SYSEX_PARTS.DATA.slice(
      0,
      SYSEX_PARTS.DATA.length - newData.length,
    );
    return [...head, ...newData];
  }
  componentDidMount() {
    this.computeSysex(this.state.pitchCorrectionOn);
  }

  render() {
    return (
      <>
        <Typography id="pitchCorrection-switch" variant="h3" gutterBottom>
          <FormattedMessage id="pitchCorrection" />{' '}
          {this.state.pitchCorrectionOn ? (
            <FormattedMessage id="enabledIt" />
          ) : (
              <FormattedMessage id="disabledIt" />
            )}
        </Typography>
        <Send
          send={this.state.send}
          onChange={(data) => this.handleSendChange(data)}
        />
        <Grid2 className="controlsSubmodule" container>
          <Grid2 size={BIG_WIDTH}>
            <Switch
              checked={this.state.pitchCorrectionOn}
              onChange={(event) => this.handlePitchCorrectionChange(event)}
              inputProps={{ 'aria-labelledby': 'pitchCorrection-switch' }}
              className="pitchCorrection-switch"
            />
          </Grid2>
        </Grid2>
        <InjectedPitchCorrectionSysexOutput
          startData={SYSEX_PARTS.START}
          endData={SYSEX_PARTS.END}
          data={SYSEX_PARTS.DATA}
        />
      </>
    );
  }
}

export default PitchCorrection;
