import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import { store } from '../store';
import { SEPARATORS, DEFAULT_SEPARATOR } from '../constants';
import { FormControlLabel, Radio, RadioGroup, Typography } from '@mui/material';

class ByteSeparator extends React.Component {
  constructor(props) {
    super(props);

    this._intl = props.intl;

    this._radios = [];
    this._createRadios();

    this.state = {
      separator: DEFAULT_SEPARATOR,
    };
    store.dispatch({
      type: 'SEPARATOR_CHANGE',
      separator: DEFAULT_SEPARATOR,
    });
  }

  _createRadios() {
    const separators = Object.keys(SEPARATORS);
    console.log(this._defaultSeparator);
    this._radios = separators.map((currentName) => (
      <FormControlLabel
        value={currentName}
        control={<Radio color="primary" />}
        label={this._intl.formatMessage({ id: `separator.${currentName}` })}
        labelPlacement="start"
        key={`key_${currentName}`}
      />
    ));
  }

  _onChange(evt) {
    this.setState({
      separator: evt.target.value,
    });

    store.dispatch({
      type: 'SEPARATOR_CHANGE',
      separator: evt.target.value,
    });
  }

  render() {
    return (
      <>
        <Typography variant="h3" gutterBottom>
          <FormattedMessage id="settings" />
        </Typography>
        <Typography variant="h4" gutterBottom>
          <FormattedMessage id="separator.header" />
        </Typography>
        <RadioGroup
          row
          name="position"
          value={this.state.separator}
          defaultValue={DEFAULT_SEPARATOR}
          onChange={(evt) => this._onChange(evt)}>
          {this._radios}
        </RadioGroup>
      </>
    );
  }
}

export default injectIntl(ByteSeparator);
