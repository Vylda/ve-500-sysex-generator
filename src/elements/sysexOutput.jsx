import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
  SPACING,
  BIG_WIDTH,
  SMALL_WIDTH,
  SUCCES_TIMEOUT,
  SEPARATORS,
} from '../constants';
import { store } from '../store';
import { Alert, Collapse, Fab, Grid2, IconButton, Typography } from '@mui/material';
import { Close, FileCopy } from '@mui/icons-material';

class SysexOutput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sysex: this.makeSysex(this.props.data),
      open: false,
      severity: 'success',
      alert: '',
    };

    this._intl = props.intl;
    if (!this._intl) {
      this._intl = {
        formatMessage: ({ id }) => {
          console.warn('Class is not injected by React-intl');
          return id || '?';
        },
      };
    }

    this.id =
      this.props.id ||
      `sysex${Math.random()}${Math.random()}`.replace(/\./g, '');

    this.subscribe();

    this.timeout = null;
  }

  subscribe() {}

  makeSysex(sysexData) {
    return [...this.props.startData, ...sysexData, ...this.props.endData]
      .map((el) => el.toUpperCase())
      .join(SEPARATORS[store.getState().separator] || ' ');
  }

  copy(event) {
    const sysexText = document.querySelector(`#${this.id}`);
    const range = document.createRange();
    range.selectNode(sysexText);
    window.getSelection().addRange(range);

    try {
      var successful = document.execCommand('copy');
      if (successful) {
        this.setState({
          severity: 'success',
          alert: this._intl.formatMessage({ id: 'copy.success' }),
        });
        this.setOpen(true);
        this.timeout = setTimeout(() => {
          this.setOpen(false);
        }, SUCCES_TIMEOUT);
      } else {
        this.setState({
          severity: 'error',
          alert: this._intl.formatMessage({ id: 'error.problem' }),
        });
      }
    } catch (err) {
      this.setState({
        severity: 'error',
        alert: this._intl.formatMessage({ id: 'error.error' }),
      });
    }

    window.getSelection().removeAllRanges();
  }

  setOpen(open) {
    this.setState({
      open,
    });
    if (!open && this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }

  render() {
    return (
      <Grid2 className="sysexSubmodule" container spacing={SPACING}>
        <Grid2 size={BIG_WIDTH}>
          <Typography className="sysexOutput" id={this.id} gutterBottom>
            {this.state.sysex}
          </Typography>
        </Grid2>

        <Grid2 size={SMALL_WIDTH}>
          <Fab
            variant="extended"
            size="small"
            color="primary"
            onClick={(event) => {
              this.copy(event);
            }}>
            <FileCopy />

            <FormattedMessage id="copy.button" />
          </Fab>
        </Grid2>

        <Collapse in={this.state.open} className="alertContainer">
          <Alert
            severity={this.state.severity}
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  this.setOpen(false);
                }}>
                <Close fontSize="inherit" />
              </IconButton>
            }>
            {this.state.alert}
          </Alert>
        </Collapse>
      </Grid2>
    );
  }
}

export default SysexOutput;
