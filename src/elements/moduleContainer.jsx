import React from "react";
import Tempo, { TEMPOS } from "./tempo";
import Key from "./key";
import Harmony from "./harmony";
import PitchCorrection from "./pitchCorrection";
import Byteseparator from "./byteseparator";

class ModuleContainer extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		if (!this.props.module) {
			return null;
		}

		let myModule = null;
		switch (this.props.module.toLowerCase()) {
			case "tempo":
				myModule = <Tempo tempo={TEMPOS.INITIAL} />;
				break;
			case "key":
				myModule = <Key />;
				break;
			case "harmony":
				myModule = <Harmony />;
				break;
			case "pitchcorrection":
				myModule = <PitchCorrection />;
				break;
			case "settings":
				myModule = <Byteseparator />;
				break;
		}

		if (!myModule) {
			return null;
		}

		return (
			<div className={`moduleContainer ${this.props.module}Module`} >
				{myModule}
			</div>
		);
	}
}


export default ModuleContainer;
