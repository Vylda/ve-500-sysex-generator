import React from 'react';
import SysexOutput from './sysexOutput';
import { FormattedMessage, injectIntl } from 'react-intl';

import Send from './send';
import { store } from '../store';
import { BIG_WIDTH } from '../constants';
import { Grid2, Switch, Typography } from '@mui/material';

const SYSEX_PARTS = {
  START: [
    'F0',
    '41',
    '10',
    '00',
    '00',
    '00',
    '55',
    '12',
    '20',
    '00',
    '60',
    '00',
  ],
  DATA: ['00', '00'],
  END: ['F7'],
};

class HarmonySysexOutput extends SysexOutput {
  subscribe() {
    store.subscribe(() => {
      let state = store.getState();
      this.setState({ sysex: this.makeSysex(state.harmony.data) });
    });
  }
}

const InjectedHarmonySysexOutput = injectIntl(HarmonySysexOutput);

class Harmony extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      harmonyOn: false,
      sysex: this.computeSysex(false),
      send: true,
    };

    store.dispatch({ type: 'HARMONY_SET_PARTS', harmonyParts: SYSEX_PARTS });
    store.dispatch({ type: 'HARMONY_SEND_CHANGED', send: this.state.send });
  }

  handleHarmonyChange(event) {
    const harmonyEnabled = event.currentTarget.checked;
    this.setState({
      harmonyOn: harmonyEnabled,
      sysex: this.computeSysex(harmonyEnabled),
    });
  }

  handleSendChange(send) {
    this.setState({
      send,
    });
    store.dispatch({ type: 'HARMONY_SEND_CHANGED', send });
  }

  computeSysex(value) {
    const data = this.getSysexData(value);
    store.dispatch({ type: 'HARMONY_CHANGED', harmonyData: data });
    return data;
  }

  getSysexData(value) {
    const hexArray = value ? ['01', '7F'] : ['00', '00'];
    return this.updateSysexData(hexArray);
  }
  updateSysexData(newData) {
    if (newData.length >= SYSEX_PARTS.DATA) {
      return [...newData];
    }

    const head = SYSEX_PARTS.DATA.slice(
      0,
      SYSEX_PARTS.DATA.length - newData.length,
    );
    return [...head, ...newData];
  }
  componentDidMount() {
    this.computeSysex(this.state.harmonyOn);
  }

  render() {
    return (
      <>
        <Typography id="harmony-switch" variant="h3" gutterBottom>
          <FormattedMessage id="harmony" />{' '}
          {this.state.harmonyOn ? (
            <FormattedMessage id="enabled" />
          ) : (
            <FormattedMessage id="disabled" />
          )}
        </Typography>

        <Send
          send={this.state.send}
          onChange={(data) => this.handleSendChange(data)}
        />

        <Grid2 className="controlsSubmodule" container>
          <Grid2 size={BIG_WIDTH}>
            <Switch
              checked={this.state.harmonyOn}
              onChange={(event) => this.handleHarmonyChange(event)}
              inputProps={{ 'aria-labelledby': 'harmony-switch' }}
              className="harmony-switch"
            />
          </Grid2>
        </Grid2>

        <InjectedHarmonySysexOutput
          startData={SYSEX_PARTS.START}
          endData={SYSEX_PARTS.END}
          data={SYSEX_PARTS.DATA}
        />
      </>
    );
  }
}

export default Harmony;
