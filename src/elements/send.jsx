import React from 'react';
import { injectIntl } from 'react-intl';
import { BIG_WIDTH } from '../constants';
import { FormControlLabel, Grid2, Switch } from '@mui/material';

class Send extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      send: props.send,
    };

    this._onChange = props.onChange;
    this._intl = props.intl;
  }

  handleChange(event) {
    const send = event.currentTarget.checked;
    this.setState({
      send,
    });
    if (this._onChange) {
      this._onChange(send);
    }
  }

  render() {
    return (
      !!navigator.requestMIDIAccess && (
        <Grid2 className="sendSubmodule" container>
          <Grid2 size={BIG_WIDTH}>
            <FormControlLabel
              control={
                <Switch
                  checked={this.state.send}
                  onChange={(event) => this.handleChange(event)}
                  inputProps={{ 'aria-labelledby': 'send-switch' }}
                  color="primary"
                />
              }
              label={this._intl.formatMessage({ id: 'midi.sendTo' })}
            />
          </Grid2>
        </Grid2>
      )
    );
  }
}

export default injectIntl(Send);
