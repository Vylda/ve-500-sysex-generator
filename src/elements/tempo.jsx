import React from 'react';
import { FormattedMessage, injectIntl } from 'react-intl';

import SysexOutput from './sysexOutput';
import Send from './send';
import { store } from '../store';
import { SPACING, BIG_WIDTH, SMALL_WIDTH } from '../constants';
import { Grid2, Slider, TextField, Typography } from '@mui/material';

export const TEMPOS = {
  INITIAL: 120,
  MIN: 10,
  MAX: 500,
};

const SYSEX_PARTS = {
  START: ['F0', '41', '10', '00', '00', '00', '55', '12', '10', '02', '60'],
  DATA: ['00', '00', '00', '00', '00', '00'],
  END: ['F7'],
};

class TempoSysexOutput extends SysexOutput {
  subscribe() {
    store.subscribe(() => {
      let state = store.getState();
      this.setState({ sysex: this.makeSysex(state.tempo.data) });
    });
  }
}

const InjectedTempoSysexOutput = injectIntl(TempoSysexOutput);

class Tempo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tempo: Number(this.props.tempo),
      sysex: this.computeSysex(this.props.tempo),
      send: true,
    };
    store.dispatch({ type: 'TEMPO_SET_PARTS', tempoParts: SYSEX_PARTS });
    store.dispatch({ type: 'TEMPO_SEND_CHANGED', send: this.state.send });
    store.dispatch({ type: 'TEMPO_TEMPO_CHANGED', tempo: this.state.tempo });

    this.marks = [
      {
        value: TEMPOS.MIN,
        label: TEMPOS.MIN.toString(),
      },
      {
        value: TEMPOS.INITIAL,
        label: TEMPOS.INITIAL.toString(),
      },
      {
        value: TEMPOS.MAX,
        label: TEMPOS.MAX.toString(),
      },
    ];

    this._intl = props.intl;
    this.preventDefault = this.preventDefault.bind(this);
    this.disableScroll = this.disableScroll.bind(this);
    this.enableScroll = this.enableScroll.bind(this);
  }

  handleTempoChange(newValue) {
    const newTempo = Math.min(
      Math.max(TEMPOS.MIN, Number(newValue)),
      TEMPOS.MAX,
    );

    this.setState({
      tempo: newTempo,
      sysex: this.computeSysex(newTempo),
    });

    store.dispatch({ type: 'TEMPO_TEMPO_CHANGED', tempo: newTempo });
  }

  handleSendChange(send) {
    this.setState({
      send,
    });
    store.dispatch({ type: 'TEMPO_SEND_CHANGED', send });
  }

  computeSysex(value) {
    const data = this.getSysexData(value);
    store.dispatch({ type: 'TEMPO_CHANGED', tempoData: data });
    return data;
  }

  getSysexData(value) {
    const hex = (value * 10).toString(16);
    const hexArray = hex.split('').map((hexValue) => `0${hexValue}`);
    const checkSumValue = hexArray.reduce(
      (acc, curVal) => acc + parseInt(curVal, 16),
      0,
    );


    const checksum = checkSumValue < 15 ? 14 - checkSumValue : 127 - checkSumValue + 15;

    hexArray.push(checksum.toString(16).padStart(2, "0"));

    return this.updateSysexData(hexArray);
  }

  updateSysexData(newData) {
    if (newData.length >= SYSEX_PARTS.DATA) {
      return [...newData];
    }

    const head = SYSEX_PARTS.DATA.slice(
      0,
      SYSEX_PARTS.DATA.length - newData.length,
    );
    return [...head, ...newData];
  }

  disableScroll() {
    document.addEventListener('wheel', this.preventDefault, {
      passive: false,
    });
  }

  enableScroll() {
    document.removeEventListener('wheel', this.preventDefault, false);
  }

  preventDefault(e) {
    e = e || window.event
    if (e.preventDefault) {
      e.preventDefault()
    }
    e.returnValue = false
  }

  componentDidMount() {
    this.computeSysex(this.state.tempo);
  }

  render() {
    return (
      <>
        <Typography id="tempo-slider" variant="h3" gutterBottom>
          <FormattedMessage id="tempo" />
        </Typography>

        <Send
          send={this.state.send}
          onChange={(data) => this.handleSendChange(data)}
        />

        <Grid2
          className="controlsSubmodule two-columns"
          container
          spacing={SPACING}
        >
          <Grid2 size={BIG_WIDTH}>
            <Slider
              value={this.state.tempo}
              aria-labelledby="tempo-slider"
              valueLabelDisplay="auto"
              step={1}
              min={TEMPOS.MIN}
              max={TEMPOS.MAX}
              marks={this.marks}
              onChange={(event, value) => {
                this.handleTempoChange(value);
              }}
            />
          </Grid2>

          <Grid2 size={SMALL_WIDTH}>
            <TextField
              id="tempo-field"
              label={this._intl.formatMessage({ id: 'tempo' })}
              onChange={(event) => {
                this.handleTempoChange(event.target.value);
              }}
              onBlur={(event) => {
                this.handleTempoChange(event.target.value);
              }}
              onMouseEnter={this.disableScroll}
              onMouseLeave={this.enableScroll}
              onWheel={(evt) => {
                const delta = evt.shiftKey ? 10 : 1;
                if (evt.deltaY < 0) {
                  this.handleTempoChange(this.state.tempo + delta);
                } else if (evt.deltaY > 0) {
                  this.handleTempoChange(this.state.tempo - delta);
                }
              }}
              slotProps={{
                htmlInput: {
                  step: 1,
                  min: TEMPOS.MIN,
                  max: TEMPOS.MAX,
                  type: 'number',
                  'aria-labelledby': 'tempo-slider',
                },
              }}
              value={this.state.tempo}
              className="numeric"
            />
          </Grid2>
        </Grid2>

        <InjectedTempoSysexOutput
          startData={SYSEX_PARTS.START}
          endData={SYSEX_PARTS.END}
          data={SYSEX_PARTS.DATA}
        />
      </>
    );
  }
}

export default injectIntl(Tempo);
