import React from 'react';

import MIDIWrapper from './midi/index';
import ModuleContainer from './elements/moduleContainer';
import { Typography } from '@mui/material';

class App extends React.Component {
  render() {
    return (
      <>
        <Typography variant="h2" gutterBottom>
          VE-500 SysEx Generator
        </Typography>
        <MIDIWrapper />
        <ModuleContainer module="tempo" />
        <ModuleContainer module="key" />
        <ModuleContainer module="harmony" />
        <ModuleContainer module="pitchcorrection" />
        <ModuleContainer module="settings" />
      </>
    );
  }
}

export default App;
