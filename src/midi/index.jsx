import React from 'react';
import { Alert, Button, Collapse, FormControl, FormHelperText, Grid2, IconButton, InputLabel, MenuItem, Select } from '@mui/material';
import { FormattedMessage, injectIntl } from 'react-intl';

import { store } from '../store';
import { SPACING, BIG_WIDTH, SMALL_WIDTH, SUCCES_TIMEOUT } from '../constants';

import note4 from './note_symbols/note4.svg';
import note8 from './note_symbols/note8.svg';
import note16 from './note_symbols/note16.svg';
import note32 from './note_symbols/note32.svg';
import { Close } from '@mui/icons-material';

class MIDIWrapper extends React.Component {
  constructor(props) {
    super(props);
    const hasMIDIAccess = !!navigator.requestMIDIAccess;
    this.state = {
      MIDIAvailable: hasMIDIAccess,
      openWarning: !hasMIDIAccess,
      MIDIOutId: '',
      MIDIOut: null,
      outputs: null,
      timeoutMultiplicator: 8,
      message: '',
      severity: '',
    };

    this._intl = props.intl;
    this._timer = null;

    this._onMIDISuccess = this._onMIDISuccess.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this._handleClick = this._handleClick.bind(this);
    this._handleSpeedChange = this._handleSpeedChange.bind(this);

    if (hasMIDIAccess) {
      navigator.requestMIDIAccess({ sysex: true }).then(this._onMIDISuccess, () => { });
    }
  }

  _onMIDISuccess(midiAccess) {
    const outputs = midiAccess.outputs;
    this.setState({
      outputs,
    });
  }

  _getMIDISelectors() {
    return Array.from(this.state.outputs.values()).reduce(
      (selects, output) => {
        selects.push(
          <MenuItem value={output.id} key={output.id}>
            {output.name}
          </MenuItem>,
        );
        return selects;
      },
      [
        <MenuItem value="" key="none">
          <FormattedMessage id="midi.none" />
        </MenuItem>,
      ],
    );
  }

  _handleChange(event) {
    const { value } = event.target;
    this.setState({
      MIDIOutId: value,
      MIDIOut: this.state.outputs.get(value),
      errorMessage: '',
    });
  }
  _handleSpeedChange(event) {
    const { value } = event.target;

    this.setState({
      timeoutMultiplicator: value,
    });
  }

  _handleClick(event) {
    const state = store.getState();
    const keys = Object.keys(state);

    this.setState({
      message: '',
      severity: '',
    });

    const data = keys.reduce((sendData, key) => {
      const substate = state[key];
      if (substate.send) {
        sendData.push(this._getSysex(substate));
      }
      return sendData;
    }, []);

    this._send(data);
  }

  _getSysex(state) {
    return [
      ...state.parts.START,
      ...state.data,
      ...state.parts.END,
    ].map((item) => Number(`0x${item}`));
  }

  _send(data) {
    if (!this.state.MIDIOut) {
      return;
    }

    try {
      this._sendMessages(data);
    } catch (error) {
      this._catchError(error);
    }
  }

  _sendMessages(data) {
    if (!data.length) {
      return;
    }
    this._setOpen({
      message: this._intl.formatMessage({ id: 'midi.sending' }),
      severity: 'info',
    })
    const { MIDIOut, timeoutMultiplicator } = this.state;
    const { tempo } = store.getState().tempo;
    const timeSpan = 60000 / (tempo * timeoutMultiplicator);
    let index = 0;
    let sequence = data[index];
    MIDIOut.send(sequence);

    const promise = new Promise((resolve) => {

      if (data.length === 1) {
        resolve()
      }
      const midiTimer = setInterval(() => {
        index++;
        if (index === data.length) {
          clearInterval(midiTimer);
          resolve();
          return;
        }

        sequence = data[index];
        MIDIOut.send(sequence);
      }, timeSpan);
    });

    promise.then(() => {
      if (this._timer) {
        clearTimeout(this._timer);
        this._timer = null;
      }

      this._setOpen({
        message: this._intl.formatMessage({ id: 'midi.success' }),
        severity: 'success',
      });

      this._timer = setTimeout(() => {
        this._timer = null;
        this._setOpen({
          message: '',
          severity: '',
        });
      }, SUCCES_TIMEOUT);
    });


  }

  _catchError(error) {
    console.error(error);

    let errorMessageId = '';

    switch (error.name) {
      case 'InvalidStateError':
        errorMessageId = 'midi.errors.disconnected';
        break;
      case 'TypeError':
        errorMessageId = 'midi.errors.badFormat';
        break;
      case 'InvalidAccessError':
        errorMessageId = 'midi.errors.cannotReceiveSysex';
        break;
      default:
        errorMessageId = 'midi.errors.default';
    }

    const message = `${this._intl.formatMessage({
      id: 'midi.errors.main',
    })} ${this._intl.formatMessage({
      id: errorMessageId,
    })}.`;

    this._setOpen({
      message,
      severity: 'error',
    });
  }

  _setOpen(open) {
    if (this._timer) {
      clearTimeout(this._timer);
      this._timer = null;
    }
    this.setState({
      message: open.message,
      severity: open.severity,
    });
  }

  _setOpenWarning(open) {
    this.setState({
      openWarning: open,
    });
  }

  render() {
    const { MIDIAvailable, outputs, MIDIOutId, timeoutMultiplicator } = this.state;

    if (!MIDIAvailable) {
      return (<div className="MIDIWrapper">
        <Collapse in={this.state.openWarning}>
          <Alert severity="warning"
            action={
              <IconButton
                aria-label="close"
                color="inherit"
                size="small"
                onClick={() => {
                  this._setOpenWarning(false);
                }}>
                <Close fontSize="inherit" />
              </IconButton>
            }
          >
            <FormattedMessage id='midi.unavailable' />
          </Alert>
        </Collapse>
      </div>);
    }
    return (
      <div className="MIDIWrapper">
        {!outputs && (
          <>
            <FormattedMessage id="midi.prepare" />
          </>
        )}

        {outputs && (
          <>
            <Grid2 className="controlsSubmodule" container spacing={SPACING}>
              <Grid2 size={BIG_WIDTH * 0.5}>
                <FormControl className="MIDISelector">
                  <InputLabel id="midi-select-helper-label">
                    <FormattedMessage id="midi.outputPort" />
                  </InputLabel>

                  <Select
                    labelId="midi-select-helper-label"
                    label={this._intl.formatMessage({ id: 'midi.outputPort' })}
                    id="midi-select-helper"
                    value={MIDIOutId}
                    onChange={this._handleChange}>
                    {this._getMIDISelectors()}
                  </Select>

                  <FormHelperText>
                    <FormattedMessage id="midi.selectPort" />
                  </FormHelperText>
                </FormControl>
              </Grid2>

              <Grid2 size={BIG_WIDTH * 0.5}>
                <FormControl className="MIDISpeedSelector">
                  <InputLabel id="midi-speed-helper-label">
                    <FormattedMessage id="midi.timeSpan" />
                  </InputLabel>

                  <Select
                    labelId="midi-speed-helper-label"
                    label={this._intl.formatMessage({ id: 'midi.timeSpan' })}
                    id="midi-speed-helper"
                    value={timeoutMultiplicator}
                    onChange={this._handleSpeedChange}
                  >
                    <MenuItem value="1" key="note_4"><img src={note4} alt="" />1/4</MenuItem>
                    <MenuItem value="2" key="note_8"><img src={note8} alt="" />1/8</MenuItem>
                    <MenuItem value="4" key="note_16"><img src={note16} alt="" />1/16</MenuItem>
                    <MenuItem value="8" key="note_32"><img src={note32} alt="" />1/32</MenuItem>
                  </Select>

                  <FormHelperText>
                    <FormattedMessage id="midi.setTimeSpan" />
                  </FormHelperText>
                </FormControl>
              </Grid2>
              <Grid2 size={SMALL_WIDTH}>
                <Button
                  variant="outlined"
                  color="primary"
                  disabled={!MIDIOutId}
                  onClick={this._handleClick}>
                  <FormattedMessage id="midi.sendSysex" />
                </Button>
              </Grid2>
            </Grid2>
            <Collapse in={!!this.state.severity && !!this.state.message}>
              <Alert
                severity={this.state.severity || 'success'}
                action={
                  <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                      this._setOpen({
                        message: '',
                        severity: '',
                      });
                    }}>
                    <Close fontSize="inherit" />
                  </IconButton>
                }>
                {this.state.message}
              </Alert>
            </Collapse>
          </>
        )}
      </div>
    );
  }
}

export default injectIntl(MIDIWrapper);
