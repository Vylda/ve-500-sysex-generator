import { object } from 'prop-types';

/**
 * Zploští strukturu objektu
 * @param {Object} nestedObject Objek ke zploštění
 * @param {String} prefix
 * @returns {Object}
 */
export function flattenObject(nestedObject, prefix = '') {
  return Object.keys(nestedObject).reduce((objects, key) => {
    const localObjs = objects;
    const value = nestedObject[key];
    const prefixedKey = prefix ? `${prefix}.${key}` : key;

    if (typeof value === 'string') {
      localObjs[prefixedKey] = value;
    } else {
      Object.assign(localObjs, flattenObject(value, prefixedKey));
    }

    return localObjs;
  }, {});
}
