const messages = {
  en: {
    tempo: 'Tempo',
    key: 'Key',
    harmony: 'Harmony',
    pitchCorrection: 'Pitch Correction',
    enabled: 'enabled',
    disabled: 'disabled',
    enabledIt: 'enabled',
    disabledIt: 'disabled',
    settings: 'Settings',
    copy: {
      button: 'Copy',
      success: 'SysEx was copied to clipboard.',
      problem: 'Sysex was not copied!',
      error: 'Ooops, copying is not possible!',
    },
    midi: {
      unavailable: 'MIDI functions are not available in this browser. Try Chrome on desktop.',
      prepare: 'MIDI is prepared…',
      sending: 'MIDI data are sending…',
      sendTo: 'Send to selected MIDI Port',
      sendSysex: 'Send SysEx',
      selectPort: 'Select output MIDI Port',
      outputPort: 'Output MIDI Port',
      none: 'None',
      success: 'MIDI data was send successful!',
      timeSpan: 'Time span for sending',
      setTimeSpan: 'Set time span (sending speed)',
      errors: {
        disconnected: 'MIDI port is disconnected',
        badFormat: 'sending data isn\'t in correct format',
        cannotReceiveSysex: 'MIDI port can\'t receive SysEx',
        default: 'an error occurred',
        main: 'Error! Failed to send all data because',
      },
    },
    separator: {
      header: 'Byte separator',
      comma: 'comma',
      space: 'space',
      commaSpace: 'comma followed by a space',
    },
  },
  cs: {
    tempo: 'Tempo',
    key: 'Tónina',
    harmony: 'Harmonie',
    pitchCorrection: 'Dolaďování',
    enabled: 'zapnuta',
    disabled: 'vypnuta',
    enabledIt: 'zapnuto',
    disabledIt: 'vypnuto',
    settings: 'Nastavení',
    copy: {
      button: 'Kopírovat',
      success: 'SysEx byl zkopírován do schránky.',
      problem: 'Sysex nebyl zkopírován!',
      error: 'Ajaj, nejde kopírovat!',
    },
    midi: {
      unavailable: 'Funkce MIDI nejsou v tomto prohlížeči dostupné. Zkuste Chrome na desktopu.',
      prepare: 'Připravuji MIDI…',
      sending: 'Odesílám MIDI…',
      sendTo: 'Odesílat do zvoleného výstupního MIDI portu',
      sendSysex: 'Odeslat SysEx',
      selectPort: 'Vyber výstupní MIDI Port',
      outputPort: 'Výstupní MIDI Port',
      none: 'Žádný',
      success: 'MIDI data byla v pořádku odeslána!',
      timeSpan: 'Časový úsek odesílání',
      setTimeSpan: 'Nastav rychlost odesílání dat',
      errors: {
        disconnected: 'MIDI port je odpojený',
        badFormat: 'data k odeslání nejsou ve vhodném formátu',
        cannotReceiveSysex: 'MIDI port neumí přijmout SysEx',
        default: 'nastala nějaká chyba',
        main: 'Chyba! Nepodařilo se odeslat všechna data, protože',
      },
    },
    separator: {
      header: 'Oddělovač bytů',
      comma: 'čárka',
      space: 'mezera',
      commaSpace: 'čárka a mezera',
    },
  },
}

export default messages;
