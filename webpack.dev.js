const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: './src/index.jsx',
	mode: "development",
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'dist'),
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/u,
				exclude: /node_modules/u,
				use: {
					loader: "babel-loader",
					options: {
						presets: [
							// [
							// 	"@babel/preset-env",
							// 	{
							// 		modules: false,
							// 		useBuiltIns: "usage",
							// 		corejs: "3",
							// 	},
							// ],
							"@babel/preset-react",
						],
						plugins: [
							"@babel/plugin-proposal-class-properties",
							"@babel/plugin-syntax-dynamic-import",
						],
					},
				},
			},
			{
				test: /\.(css|less)$/u,
				use: [
					"style-loader",
					{
						loader: "css-loader",
						options: {
							importLoaders: 2,
						},
					},
					"postcss-loader",
					"less-loader",
				],
			},
			{
				test: /\.(png|svg|jpg|gif)$/u,
				use: [
					{
						loader: "url-loader",
						options: {
							limit: 2048,
						},
					},
				],
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/u,
				use: "file-loader",
			},
		],
	},
	resolve: {
		extensions: ['.js', '.jsx']
	},
	plugins: [
		new HtmlWebpackPlugin({
      template: './src/template.html',
      title: 'VE-500 SysEx Generator',
      scriptLoading: 'defer',
			favicon: './src/favicon.svg',
		}),
	],
	devServer: {
		compress: true,
		open: true,
    port: 9000,
  },
};
